require 'colorize' # Found how to output color to terminal

class Code
  attr_reader :pegs

  PEGS = {
    'r' => 'Red',
    'g' => 'Green',
    'b' => 'Blue',
    'y' => 'Yellow',
    'o' => 'Orange',
    'p' => 'Purple'
  }.freeze

  def initialize(pegs)
    return @pegs = pegs.map(&:downcase) if pegs.is_a?(Array)
    raise 'Not an Array'
  end

  def [](idx)
    @pegs[idx]
  end

  def self.parse(code) # Ensures user Submissions are valid and length of 4
    pegs_arr = code.chars
    pegs_arr.map do |letter|
      if PEGS.key?(letter.downcase) == false || pegs_arr.size != 4
        raise 'Invalid Submission. Please Retry'
      end
      letter.downcase
    end
    Code.new(pegs_arr)
  end

  def self.random
    keys = PEGS.keys
    random_code = []
    4.times { random_code << keys[rand(6)]}
    self.new(random_code)
  end

  def exact_matches(pegs)
    count = 0
    @pegs.each_index { |idx| count += 1 if @pegs.[](idx) == pegs.[](idx) }
    count
  end

  def ==(code)
    return false unless code.is_a?(Code)
    return true if self.pegs == code.pegs
    false
  end

  def near_matches(code)
    count_arr = []
    code.pegs.each_with_index do |peg, idx|
      count_arr << peg if peg != @pegs[idx] && @pegs.include?(peg)
    end
    count_arr.uniq.size
  end
end

class Game
  attr_reader :secret_code, :player_wins, :game_over

  def initialize(secret_code = nil)
    if secret_code.nil?
      @secret_code = Code.random
    elsif secret_code.is_a?(Code)
      @secret_code = secret_code
    else
      @secret_code = Code.parse(secret_code) # Enter code as string ex: 'brgb'
      # Also checks to see that 'user submitted' secret code is valid using Code::parse
    end
    @guesses_left = 10
    @player_wins = false
  end

  def get_guess
    puts "\nPlease enter your guess!".yellow
    puts "Only chose from r, g, b, y, o or p! Example entry: 'rbyp'".yellow
    begin
      Code.parse(gets.chomp)
    rescue
      puts 'Invalid Submission. Please Retry!'
      retry
    end
  end

  def game_over?
    if @player_wins
      puts "Correct! YOU WIN!! With #{@guesses_left} guesses to spare!".green
    elsif @guesses_left.zero?
      puts 'Sorry, You Lose!'.red
    else
      puts "You have #{@guesses_left} guess left!".yellow
      play
    end
  end

  def play
    player_guess = get_guess
    @player_wins = true if player_guess.==(@secret_code)
    @guesses_left -= 1
    display_matches(player_guess)
    game_over?
  end

  def display_matches(player_guess)
    puts "You have #{@secret_code.exact_matches(player_guess)} exact matches".yellow
    puts "You have #{@secret_code.near_matches(player_guess)} near matches".yellow
  end

  def red
    colorize(31)
  end

  def green
    colorize(32)
  end

  def yellow
    colorize(33)
  end
end
